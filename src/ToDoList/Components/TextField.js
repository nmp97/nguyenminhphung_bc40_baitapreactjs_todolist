import styled from 'styled-components';

const Input = styled.input`
    border:1px solid ${(props) => props.theme.color}
    min-height:35px;
    height:35px;
    font-size:16px;
    width:auto;
    display:initial;
    &:focus-visible{
      outline:${(props) => props.theme.color} solid 1px;
    }
`;

const Label = styled.span`
    color: ${(props) => props.theme.color}
    width:auto
`;

export const TextField = ({ label, ...props }) => {
  return (
    <span>
      <Label>{label}</Label>
      <br />
      <Input {...props} />
    </span>
  );
};
