import React from 'react';
import styled from 'styled-components';

export const Link = ({ className, children, ...restProps }) => {
  <a href="g" className={className} {...restProps}>
    {children}
  </a>;
};

export const StyledLink = styled(Link)`
  background-color: red;
  font-weight: bold;
  color: #fff;
`;
