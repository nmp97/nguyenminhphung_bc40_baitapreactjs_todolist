import { DefaultTheme } from '../../Themes/DefaultTheme';
import { localToDoServ } from '../../services/localTodoServ';
import { themes } from '../../Themes/themes';
import {
  ADD_TASK,
  CHANGE_THEME,
  DELETE_TASK,
  DONE_TASK,
  EDIT_TASK,
  UPDATE_TASK,
} from '../../constants/toDoListConstant';

const initialState = {
  themeToDoList: DefaultTheme,
  toDoTask: localToDoServ.get() || [],
  editTask: { id: '-1', taskName: '', done: false },
};

export const toDoListReducer = (state = initialState, action) => {
  switch (action.type) {
    case ADD_TASK: {
      let taskUpdate = [...state.toDoTask];
      let index = taskUpdate.findIndex((item) => item.taskName === action.payload.taskName);

      console.log('add', taskUpdate);

      if (action.payload.taskName.trim() === '') {
        alert('Vui lòng nhập task!');
      } else if (index === -1) {
        taskUpdate.push(action.payload);
        localToDoServ.set(taskUpdate);
      } else {
        alert('Task đã tồn tại!');
      }
      return { ...state, toDoTask: taskUpdate };
    }
    case CHANGE_THEME: {
      let theme = themes.find((theme) => theme.id === action.payload);
      if (theme) {
        state.themeToDoList = theme.theme;
      }

      return { ...state };
    }
    case DELETE_TASK: {
      let updateToDoTask = [...state.toDoTask];

      state.toDoTask = updateToDoTask.filter((item) => item.id !== action.payload);

      localToDoServ.set(state.toDoTask);

      return { ...state };
    }
    case DONE_TASK: {
      let updateToDoTask = [...state.toDoTask];
      let index = updateToDoTask.findIndex((item) => item.id === action.payload);

      updateToDoTask[index].done = true;

      state.toDoTask = updateToDoTask;

      localToDoServ.set(state.toDoTask);

      return { ...state };
    }
    case EDIT_TASK: {
      let updateToDoTask = [...state.toDoTask];
      let index = updateToDoTask.findIndex((item) => item.id === action.payload);
      let updateEditTask = updateToDoTask[index];

      return { ...state, editTask: updateEditTask };
    }
    case UPDATE_TASK: {
      state.editTask = { ...state.editTask, taskName: action.payload };
      let newToDoTask = [...state.toDoTask];

      console.log('update', newToDoTask);

      let index = newToDoTask.findIndex((item) => item.id === state.editTask.id);

      if (index !== -1) {
        newToDoTask[index].taskName = state.editTask.taskName;
      }

      state.editTask = { id: '-1', taskName: '', done: false };

      localToDoServ.set(newToDoTask);
      return { ...state, toDoTask: newToDoTask };
    }
    default:
      return state;
  }
};
