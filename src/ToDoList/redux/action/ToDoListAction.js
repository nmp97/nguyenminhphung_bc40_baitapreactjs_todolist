import {
  ADD_TASK,
  CHANGE_THEME,
  DELETE_TASK,
  DONE_TASK,
  EDIT_TASK,
  UPDATE_TASK,
} from '../../constants/toDoListConstant';

export const addToTaskAction = (task) => {
  return {
    type: ADD_TASK,
    payload: task,
  };
};

export const changeThemeAction = (themeId) => {
  return {
    type: CHANGE_THEME,
    payload: themeId,
  };
};

export const deleteTaskAction = (id) => {
  return {
    type: DELETE_TASK,
    payload: id,
  };
};

export const doneTaskAction = (id) => {
  return {
    type: DONE_TASK,
    payload: id,
  };
};

export const editTaskAction = (id) => {
  return {
    type: EDIT_TASK,
    payload: id,
  };
};

export const updateTaskAction = (task) => {
  return {
    type: UPDATE_TASK,
    payload: task,
  };
};
