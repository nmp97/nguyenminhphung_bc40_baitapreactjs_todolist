import { TO_DO_LOCAL } from '../constants/toDoListConstant';

export const localToDoServ = {
  get: () => {
    let dataJSON = localStorage.getItem(TO_DO_LOCAL);

    return JSON.parse(dataJSON);
  },

  set: (newTask) => {
    let dataJSON = JSON.stringify(newTask);

    return localStorage.setItem(TO_DO_LOCAL, dataJSON);
  },

  remove: () => {
    localStorage.removeItem(TO_DO_LOCAL);
  },
};
