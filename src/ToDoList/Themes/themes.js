import { DarkTheme } from './DarkTheme';
import { DefaultTheme } from './DefaultTheme';
import { LightTheme } from './LightTheme';

export const themes = [
  { id: '1', name: 'Dark theme', theme: DarkTheme },
  { id: '2', name: 'Light theme', theme: LightTheme },
  { id: '3', name: 'Default theme', theme: DefaultTheme },
];
