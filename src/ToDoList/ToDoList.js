import React, { Component } from 'react';
import { ThemeProvider } from 'styled-components';
import { Container } from './Components/Container';
import { Dropdown } from './Components/Dropdown';
import { Heading3, Heading4 } from './Components/Heading';
import { TextField } from './Components/TextField';
import { Button } from './Components/Button';
import { Table, Thead, Tr, Th } from './Components/Table';
import { connect } from 'react-redux';
import {
  addToTaskAction,
  changeThemeAction,
  deleteTaskAction,
  doneTaskAction,
  editTaskAction,
  updateTaskAction,
} from './redux/action/ToDoListAction';
import { themes } from './Themes/themes';
import { DefaultTheme } from './Themes/DefaultTheme';

class ToDoList extends Component {
  state = {
    taskName: '',
    disabled: true,
  };

  renderToDoTask = () => {
    return this.props.toDoTask
      .filter((task) => !task.done)
      .map((item, index) => {
        return (
          <Tr key={index}>
            <Th>{item.taskName}</Th>
            <Th className="text-end">
              <Button
                onClick={() => {
                  this.setState({ disabled: false }, () => {
                    this.props.handleEditTask(item.id);
                  });
                }}
              >
                <i className="fa fa-edit"></i>
              </Button>
              <Button
                onClick={() => {
                  this.props.handleDoneTask(item.id);
                }}
              >
                <i className="fa fa-check"></i>
              </Button>
              <Button
                onClick={() => {
                  this.props.handleDeleteTask(item.id);
                }}
              >
                <i className="fa fa-trash-alt"></i>
              </Button>
            </Th>
          </Tr>
        );
      });
  };
  renderToDoCompleted = () => {
    return this.props.toDoTask
      .filter((task) => task.done)
      .map((item, index) => {
        return (
          <Tr key={index}>
            <Th>{item.taskName}</Th>
            <Th className="text-end">
              <Button
                onClick={() => {
                  this.props.handleDeleteTask(item.id);
                }}
              >
                <i className="fa fa-trash-alt"></i>
              </Button>
            </Th>
          </Tr>
        );
      });
  };

  renderThemes = () => {
    return themes.map((theme, index) => {
      return (
        <option value={theme.id} key={index}>
          {theme.name}
        </option>
      );
    });
  };

  // componentWillReceiveProps(newProps) {
  //   this.setState({
  //     taskName: newProps.editTask.taskName,
  //   });
  // }

  render() {
    return (
      <ThemeProvider theme={this.props.themeToDoList || DefaultTheme}>
        <Container className="w-50">
          <Dropdown
            onChange={(e) => {
              let { value } = e.target;
              this.props.handleChangeTheme(value);
            }}
          >
            {this.renderThemes()}
          </Dropdown>
          <Heading3 className="mt-3">To Do List</Heading3>
          <TextField
            value={this.state.taskName}
            className="w-50"
            label="Task name"
            onChange={(e) => {
              this.setState({ taskName: e.target.value });
            }}
          />
          <Button
            className="ms-3"
            onClick={() => {
              let { taskName } = this.state;
              let newTask = {
                id: Math.floor(Math.random() * 1000).toString(),
                taskName,
                done: false,
              };
              this.setState({ taskName: '' });
              this.props.handleAddToTask(newTask);
            }}
          >
            <i className="fa-solid fa-plus"></i>
            Add task
          </Button>
          <Button
            disabled={this.state.disabled}
            className="ms-3"
            onClick={() => {
              let { taskName } = this.state;

              this.setState(
                {
                  disabled: true,
                  taskName: '',
                },
                () => {
                  this.props.handleUpdateTask(taskName);
                }
              );
            }}
          >
            <i className="fa-solid fa-upload"></i>
            Update task
          </Button>
          <hr />
          <Heading4 className="my-2">Task to do</Heading4>
          <Table>
            <Thead>{this.renderToDoTask()}</Thead>
          </Table>
          <hr />
          <Heading4 className="my-2">Task completed</Heading4>
          <Table>
            <Thead>{this.renderToDoCompleted()}</Thead>
          </Table>
        </Container>
        ;
      </ThemeProvider>
    );
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevProps.editTask.id !== this.props.editTask.id) {
      this.setState({
        taskName: this.props.editTask.taskName,
      });
    }
  }
}

const mapStateToProps = (state) => {
  return {
    themeToDoList: state.toDoListReducer.themeToDoList,
    toDoTask: state.toDoListReducer.toDoTask,
    editTask: state.toDoListReducer.editTask,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    handleAddToTask: (task) => {
      dispatch(addToTaskAction(task));
    },
    handleChangeTheme: (themeId) => {
      dispatch(changeThemeAction(themeId));
    },
    handleDeleteTask: (id) => {
      dispatch(deleteTaskAction(id));
    },
    handleDoneTask: (id) => {
      dispatch(doneTaskAction(id));
    },
    handleEditTask: (id) => {
      dispatch(editTaskAction(id));
    },
    handleUpdateTask: (task) => {
      dispatch(updateTaskAction(task));
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ToDoList);
