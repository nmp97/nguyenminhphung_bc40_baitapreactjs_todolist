import styled from 'styled-components';

export const Container = styled.div`
  width: 100%;
  padding-right: 24px;
  padding-left: 24px;
  margin-right: auto;
  margin-left: auto;
  border: 3px solid ${(props) => props.theme.borderColor};
`;
